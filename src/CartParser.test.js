import CartParser from './CartParser';
import { readFileSync, writeFileSync, unlink } from 'fs';

let parser;
const cart = {
    "items": [
        {
            "id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",
            "name": "Mollis consequat",
            "price": 9,
            "quantity": 2
        },
        {
            "id": "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
            "name": "Tvoluptatem",
            "price": 10.32,
            "quantity": 1
        },
        {
            "id": "33c14844-8cae-4acd-91ed-6209a6c0bc31",
            "name": "Scelerisque lacinia",
            "price": 18.9,
            "quantity": 1
        },
        {
            "id": "f089a251-a563-46ef-b27b-5c9f6dd0afd3",
            "name": "Consectetur adipiscing",
            "price": 28.72,
            "quantity": 10
        },
        {
            "id": "0d1cbe5e-3de6-4f6a-9c53-bab32c168fbf",
            "name": "Condimentum aliquet",
            "price": 13.9,
            "quantity": 1
        }
    ],
    "total": 348.32
};

beforeEach(() => {
	parser = new CartParser();

});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.
    it('should throw error if no file path', () => {
    	function emptyPath() {
            parser.readFile("");
        }
        expect(emptyPath).toThrow(/ENOENT/);
    });

    it('should read some string data from file path', () => {

        expect(typeof parser.readFile("./samples/cart.csv")).toBe('string');
    });

    it('should return data from file', () => {
        writeFileSync("test.csv", "test", function(err){
            if(err)throw err;
        });

        expect(parser.readFile("test.csv")).toEqual("test");

        unlink('test.csv',function(err){
            if(err) return console.log(err);
        });
    });

    it('should return error if header is not valid', () => {

        expect(parser.validate('Product name,P,Quantity\n' +
            'Mollis consequat,9.00,2')[0]['message']).toEqual('Expected header to be named \"Price\" but received P.');
    });
    it('should return error if number is not valid (is not positive)', () => {

        expect(parser.validate('Product name,Price,Quantity\n' +
            'Mollis consequat,9.00,-2')[0]['message']).toEqual('Expected cell to be a positive number but received \"-2\".');
    });
    it('should return error if get empty product name', () => {

        expect(parser.validate('Product name,Price,Quantity\n' +
            ',9.00,2')[0]['message']).toEqual('Expected cell to be a nonempty string but received \"\".');
    });
    it('should return error if get not all cells', () => {

        expect(parser.validate('Product name,Price,Quantity\n' +
            'Mollis consequat,2')[0]['message']).toEqual('Expected row to have 3 cells but received 2.');
    });
    it('should return empty array if all valid', () => {

        expect(parser.validate('Product name,Price,Quantity\n' +
            'Mollis consequat,9.00,2')).toStrictEqual([]);
    });
    it('should create item from line', () => {
        const data = 'Mollis consequat,9.00,2';
        expect(parser.parseLine(data)['name']).toBe(cart['items'][0]['name']);
    });

    it('should count cart total', () => {
        const data = [{"price": 2, "quantity": 2}, {"price": 3, "quantity": 3}];
        expect(parser.calcTotal(data)).toBe(13);
    });

});

describe('CartParser - integration test', () => {
	//Add your integration test here.

    test('should parse the file and return items like an objects', () => {
        expect(parser.parse("./samples/cart.csv")['items'].length).toBe(5);
    });
});